<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Completion Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('completion_date', 'Completion Date:') !!}
    {!! Form::date('completion_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Project Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_Project', 'Id Project:') !!}
    {!!Form::select('id_Project', $projects, null, ['placeholder' => 'Select a project...', 'class' => 'form-control']) !!}

</div>

<!-- Id Label Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_label', 'Id Label:') !!}
    {!! Form::number('id_label', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tasks.index') !!}" class="btn btn-default">Cancel</a>
</div>
