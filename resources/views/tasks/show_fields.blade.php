<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $task->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $task->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $task->description !!}</p>
</div>

<!-- Completion Date Field -->
<div class="form-group">
    {!! Form::label('completion_date', 'Completion Date:') !!}
    <p>{!! $task->completion_date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $task->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $task->updated_at !!}</p>
</div>

<!-- Id Project Field -->
<div class="form-group">
    {!! Form::label('id_Project', 'Id Project:') !!}
    <p>{!! $task->id_Project !!}</p>
</div>

<!-- Id Label Field -->
<div class="form-group">
    {!! Form::label('id_label', 'Id Label:') !!}
    <p>{!! $task->id_label !!}</p>
</div>

