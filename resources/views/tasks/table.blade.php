<table class="table table-responsive" id="tasks-table">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Completion Date</th>
        <th></th>Project</th>
        <th>Label</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($tasks as $task)
        <tr>
            <td>{!! $task->name !!}</td>
            <td>{!! $task->description !!}</td>
            <td>{!! $task->completion_date !!}</td>
            <td>{!! $task->project->name !!}</td>
            <td><div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
                         aria-valuenow="{!! $task->label->color !!}" aria-valuemin="0" aria-valuemax="100" style="width:{!! $task->label->color !!}%">
                        {!! $task->label->color !!}% Complete (info)
                    </div>
                </div></td>
            <td>
                {!! Form::open(['route' => ['tasks.destroy', $task->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tasks.show', [$task->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tasks.edit', [$task->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>