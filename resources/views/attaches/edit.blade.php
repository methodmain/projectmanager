@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Attach
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($attach, ['route' => ['attaches.update', $attach->id], 'method' => 'patch']) !!}

                        @include('attaches.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection