<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $member->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $member->created_at !!}</p>
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $member->id !!}</p>
</div>

<!-- Id Task Field -->
<div class="form-group">
    {!! Form::label('id_Task', 'Id Task:') !!}
    <p>{!! $member->id_Task !!}</p>
</div>

