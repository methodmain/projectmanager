<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Task Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_Task', 'Id Task:') !!}
    {!! Form::number('id_Task', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('members.index') !!}" class="btn btn-default">Cancel</a>
</div>
