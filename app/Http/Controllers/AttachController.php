<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAttachRequest;
use App\Http\Requests\UpdateAttachRequest;
use App\Repositories\AttachRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AttachController extends AppBaseController
{
    /** @var  AttachRepository */
    private $attachRepository;

    public function __construct(AttachRepository $attachRepo)
    {
        $this->attachRepository = $attachRepo;
    }

    /**
     * Display a listing of the Attach.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->attachRepository->pushCriteria(new RequestCriteria($request));
        $attaches = $this->attachRepository->all();

        return view('attaches.index')
            ->with('attaches', $attaches);
    }

    /**
     * Show the form for creating a new Attach.
     *
     * @return Response
     */
    public function create()
    {
        return view('attaches.create');
    }

    /**
     * Store a newly created Attach in storage.
     *
     * @param CreateAttachRequest $request
     *
     * @return Response
     */
    public function store(CreateAttachRequest $request)
    {
        $input = $request->all();

        $attach = $this->attachRepository->create($input);

        Flash::success('Attach saved successfully.');

        return redirect(route('attaches.index'));
    }

    /**
     * Display the specified Attach.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $attach = $this->attachRepository->findWithoutFail($id);

        if (empty($attach)) {
            Flash::error('Attach not found');

            return redirect(route('attaches.index'));
        }

        return view('attaches.show')->with('attach', $attach);
    }

    /**
     * Show the form for editing the specified Attach.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $attach = $this->attachRepository->findWithoutFail($id);

        if (empty($attach)) {
            Flash::error('Attach not found');

            return redirect(route('attaches.index'));
        }

        return view('attaches.edit')->with('attach', $attach);
    }

    /**
     * Update the specified Attach in storage.
     *
     * @param  int              $id
     * @param UpdateAttachRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAttachRequest $request)
    {
        $attach = $this->attachRepository->findWithoutFail($id);

        if (empty($attach)) {
            Flash::error('Attach not found');

            return redirect(route('attaches.index'));
        }

        $attach = $this->attachRepository->update($request->all(), $id);

        Flash::success('Attach updated successfully.');

        return redirect(route('attaches.index'));
    }

    /**
     * Remove the specified Attach from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $attach = $this->attachRepository->findWithoutFail($id);

        if (empty($attach)) {
            Flash::error('Attach not found');

            return redirect(route('attaches.index'));
        }

        $this->attachRepository->delete($id);

        Flash::success('Attach deleted successfully.');

        return redirect(route('attaches.index'));
    }
}
