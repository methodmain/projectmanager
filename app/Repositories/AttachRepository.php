<?php

namespace App\Repositories;

use App\Models\Attach;
use InfyOm\Generator\Common\BaseRepository;

class AttachRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'url',
        'id_Task'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Attach::class;
    }
}
