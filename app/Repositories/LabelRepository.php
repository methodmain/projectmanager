<?php

namespace App\Repositories;

use App\Models\Label;
use InfyOm\Generator\Common\BaseRepository;

class LabelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'color'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Label::class;
    }
}
